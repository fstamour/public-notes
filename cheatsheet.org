:PROPERTIES:
:ID:       178e19d6-1e18-43d0-98a3-2792c09687cc
:END:
#+title: Programming language cheatsheet

I work with many programming languages, it is sometimes hard to
remember the names basic stuff as they differ just a little bit across
languages.

* Cheatsheet: Array
:PROPERTIES:
:ID:       c807a0dd-e53a-402e-940c-4c540029a841
:END:

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

| What                                                        | JavaScript                                |
|-------------------------------------------------------------+-------------------------------------------|
| Documentation                                               | [[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array][Array (MDN)]]                               |
| Creating a new array from two arrays                        | ~const newArray = array1.concat(array2);~ |
| Append an array to an existing one (destructively)          | ~array1.push(...array2);~                 |
| Add an element to the end of an array (destructively)       | ~.push()~                                 |
| Remove the last element of an array (destructively)         | ~.pop()~                                  |
| Add an element to the beginning of an array (destructively) | ~.unshift()~                              |
| Remove the first element of an array (destructively)        | ~.shift()~                                |
| Remove an item at index (destructively)                     | ~.splice(i, 1)~                           |

* Cheatsheet: Map, Hash, Object, Dict

This one is annoying because each language have different names for
the abstract data type and then have even more names for a bunch of
variants.

| Name       | Language(s) |
|------------+-------------|
| map        | js          |
| dict       | python      |
| object     | js          |
| hash       | ruby        |
| hash table | lisp        |

* Cheatsheet: Sets

| What                  | JavaScript  | Python  | Lisp                         | Ruby |
|-----------------------+-------------+---------+------------------------------+------|
| Creating an empty set | ~new Set()~ | ~Set()~ | ~'()~ or ~(make-hash-table)~ |      |

* Cheatsheet: Iteration

In javascript, the ~for ... of~ syntax does *not* work for object, as
they are not iterable.

To get something you can iterate on, you can use one of these static
methods:
- ~Object.entries(...)~,
- ~Object.keys(...)~, or
- ~Object.values(...)~

Example:
#+begin_src js
const obj = { a: 1, b: 2, c: 3 };
for(let [key, value] of Object.entries(obj)) ...
#+end_src

* Cheatsheet: Reflection

** Check if a variable is a string

#+begin_src javascript
const isString = x => typeof x === 'string' || x instanceof String;
#+end_src

#+begin_src python
isinstance(x, str)
#+end_src

#+begin_src lisp
(stringp x)
#+end_src

** Get the object's class
:PROPERTIES:
:ID:       de3aff97-4093-417c-a1ce-d2e7462ed882
:END:

#+begin_src javascript
x.constructor.name
#+end_src

#+begin_src python
x.__class__
x.__class__.__name__
#+end_src

#+begin_src lisp
(class-of x)
(class-name (class-of x))
#+end_src

** JavaScript specific

*** Check if something is a DOM element
:PROPERTIES:
:ID:       e27441b7-7570-4412-8ba3-eb30261fae72
:END:

#+begin_src javascript
const isElement = x => x.tagName || x.nodeName;
#+end_src

* Concurrency

** Cheatsheet: Run something with a delay
:PROPERTIES:
:ID:       fded49a2-6bcb-4477-aff8-bb0d9119c6ba
:END:

#+begin_src javascript
const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
#+end_src

#+begin_src lisp
(defun delay (seconds function)
  (bt:make-thread
   (lambda ()
     (sleep seconds)
     (funcall function))))
#+end_src

* Cheatsheet: Cross-language Glossary
:PROPERTIES:
:ID:       51748d1c-d868-4b71-ac38-e71b01d9f906
:END:

N.B. Those terms are not always a perfect match.

| Javascript                  | Python                  | Lisp                | Ruby                                             |
|-----------------------------+-------------------------+---------------------+--------------------------------------------------|
| default function parameters | default argument values | optional parameters | =arguments with default expression are optional= |
| throw                       | raise                   | signal or error     | raise                                            |
| destructuring assignment    |                         | destructuring-bind  | pattern matching + pin operator                  |
| spread syntax               | unpacking               | apply               | splat operator                                   |
| rest parameters             | packing                 | rest parameters     |                                                  |
|                             |                         | call-next-method    | argument forwarding                              |
|                             | structural matching     |                     | pattern matching                                 |
|                             |                         |                     | rightward assignment                             |
|                             |                         |                     | destructuring method                             |
|                             |                         | ∄                   | pin operator                                     |
|                             |                         |                     |                                                  |

* Cheatsheet: Monads

#+begin_quote
Monads allow ordered computation within functional programming
#+end_quote

** Interface

- unit / constructor / lift / pure
- bind / flatMap

** Laws

- left identity =bind(unit(x),  f) ≍ f(x)=
- right identity =bind(m, unit(x)) ≍ m=
- associativity =flatMap(flatMap(m, f), g) ≍ flatMap(m, x => flatMap(x))=

** Common types of monads

- maybe, option, nullable
- option, either
- exceptions
- list, array
- state
- io
  - reader monad
- monadic parser [combinator]
- free monads
  - AFAIU, they just build up stuff, they don't do

** See also

- [[https://en.wikipedia.org/wiki/Arrow_%28computer_science%29][Arrows]] are a generalization of monads
  - arrows provide a referentially transparent way of expressing
    relationships between logical steps in a computation.
  - Unlike monads, arrows don't limit steps to having one and only one
    input.

* bash's heredoc and herestring syntax
:PROPERTIES:
:ID:       a12f507e-b3c4-4d74-a14e-1b344a352441
:END:

#+begin_src bash :results verbatim drawer
cat <<EOF
  as is
EOF
#+end_src

#+RESULTS:
:results:
  as is
:end:

#+begin_src bash :results verbatim drawer
cat <<-EOF
		<<- removes leading tabs
	but not
    spaces
EOF
#+end_src

#+RESULTS:
:results:
<<- removes leading tabs
but not
    spaces
:end:

* bash: slugify - creating a URL-friendly string

#+begin_src bash
slugify () {
    echo "$1" | iconv -t ascii//TRANSLIT | sed -r 's/[~\^]+//g; s/[^a-zA-Z0-9]+/-/g; s/^-+\|-+$//g' | tr A-Z a-z
}
#+end_src

* Related notes
:PROPERTIES:
:ID:       65a30a45-f9af-41a7-b52a-a88f409e50cc
:END:

- [[id:de9f1915-0a68-4396-a00e-9893dee341ab][Non-interactive apt]]
- [[id:a30fe4ca-3b97-4fc6-aeab-e2baafda9b4c][Fish - the friendly interactive shell]]
- [[id:0f3ce8ff-849c-4117-955b-63944086ab5b][Forth]]
- [[id:61d7a86b-e530-4799-9a29-2a389c1ac980][Systemd]]
- [[id:81cbf771-a005-4f7e-bf5b-0dfc32a7be5e][TypeScript]]
