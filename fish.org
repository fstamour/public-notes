:PROPERTIES:
:ID:       a30fe4ca-3b97-4fc6-aeab-e2baafda9b4c
:END:
#+title: Fish - the friendly interactive shell

* Fish shell

- https://fishshell.com/
- save a function with ~funcsave the_function~

** Fish: Remove the greeting message
:PROPERTIES:
:ID:       40d0a197-6f39-4836-9f8b-4c6c0b2d9df4
:END:

#+begin_src fish
set -U fish_greeting ""
#+end_src

Relatedly, you can define a function called ~fish_greeting~ (same name
as the variable) if you want to run some custom commands (like
~fortune~, or ~task~) when starting an _interactive_ shell.

** Fish: Variables
:PROPERTIES:
:ID:       3d636762-8a92-4820-b660-243bbec3f273
:END:

- use =set -h= for more info
- there are 4 scopes: local, function, global and universal
- global scope is just like global scope in other languages
- universal variables are shared and persisted; this means they can be
  slightly slower than global variables because they need to be
  written to disk
- set =set VARIABLE_NAME VALUES=
- unset =set -e VARIABLE_NAME=
- export with =-x=
- test if a variable exists =set -q VARIABLE_NAME=

*** Fish: Arrays
:PROPERTIES:
:ID:       710d699a-b145-450d-bbcc-8e02b7ba0af1
:END:

- are 1 indexed
- are *not* created differently from other variables
- are indexed with =$var[1]=
- remove an item =set -e var[idx]=
- can index from the end =$var[-1]=
- index range is supported =$var[1..4]=, bounds are inclusive
