:PROPERTIES:
:ID:       bd1fc08f-26f3-4244-b745-cd61cd8e4959
:END:
#+title: Notes
#+options: toc:nil
#+options: html-postamble:nil

* About

This is just a bunch of notes for myself, it's public in order to be
easy to access from anywhere. It might accidently be useful to other
people. If you feel like it, you can contribute [[https://gitlab.com/fstamour/public-notes][here]].

* Quick Links

** Source Forges

- [[https://github.com/fstamour][My GitHub profile]]
- [[https://gitlab.com/fstamour][My GitLab's profile]]
- [[https://codeberg.org/fstamour/][My Codeberg profile]]

** Repositories

- [[https://gitlab.com/fstamour/public-notes][This page's repository]]
- [[https://gitlab.com/fstamour/catchall][Catchall's repo]]
- [[https://fstamour.gitlab.io/breeze/][Breeze's homepage]]
- [[https://gitlab.com/fstamour/breeze][Breeze's repo]]
- [[https://gitlab.com/fstamour/simpbin][Simpbin's repo]]

# - [[http://www.fstamour.com/recettes/index.html][Recettes]]

* Indexes

  #+toc: headlines

  #+toc: tables

** Other pages

#+INCLUDE: sitemap.org :lines "3-"
