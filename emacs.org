:PROPERTIES:
:ID:       b55fd406-e831-4787-8634-49a66bf0a449
:END:
#+title: Notes about Emacs

* Emacs: some useful commands and bindings
:PROPERTIES:
:ID:       23a404ca-f812-43e5-9056-b38c094c0ff5
:END:

- the command =ispell-word= is bound to =M-$= by default, it
  spell-check the current word.
- the command =kmacro-edit-lossage= is bound to =C-x C-k l= by
  default, it lets you edit the last 300 keystrokes as a keyboard
  macro.
- =C-a= goes to the beginning of the line, but very often =M-m= is
  more useful as it goes to the first non-whitespace character on the
  line.

* Emacs Org-Mode
:PROPERTIES:
:ID:       8e7fbed4-26d4-42c5-948e-7dd392c84c69
:END:

** [[https://orgmode.org/worg/dev/org-syntax.html][Org-mode syntax]]
:PROPERTIES:
:ID:       101d4633-8f86-4c46-a895-ad0a174b89c6
:END:
