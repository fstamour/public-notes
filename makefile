
SRC := $(wildcard *.org)
HTML := $(SRC:%.org=public/%.html)
SHELL := bash

.PHONY: org-publish
org-publish: $(SRC)
	scripts/export.sh

$(HTML) &: $(SRC)
	scripts/export.sh

define \n


endef


define dockerfile :=
# syntax=docker/dockerfile:1
FROM docker.io/silex/emacs:29.2-ci
WORKDIR /notes
ENTRYPOINT sh

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update
RUN apt install gcc -y

COPY scripts/install-dependencies.el install-dependencies.el
RUN emacs -Q --batch --load install-dependencies.el
endef

.PHONY: ci
ci:
	echo $$'$(subst ',\',$(subst $(\n),\n,$(dockerfile)))' | docker build -f - -t public-notes:dev .
	echo 'scripts/export.sh' | docker run -i --rm -v $$PWD:/notes public-notes:dev

.PHONY: clean
clean:
	-rm -r public/


## ;; (emacsql-sqlite "~/.emacs.d/org-roam.db")
