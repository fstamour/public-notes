#!/usr/bin/env bash

# Redirect stderr into stdout, to avoid some weird print order issues
# in ci
exec 2>&1

# Print every commands
# set -x

# Stop on errors
set -ueo pipefail

cd "$(git rev-parse --show-toplevel)"

for org_file in *.org; do
    if ! head -1 $org_file | grep -q -i '#+title: '; then
        echo "$org_file is missing a #+title"
    fi
done
