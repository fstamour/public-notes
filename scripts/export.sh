#!/usr/bin/env bash
#
# This script generates the html pages
#

set -eux

cd "$(git rev-parse --show-toplevel)"

emacs --version

emacs -Q --batch --load scripts/org-publish-project.el --kill
