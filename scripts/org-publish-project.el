(require 'cl-lib)

(message "%s" (emacs-version))
(message "Features: %s"
         (mapconcat (lambda (x) (format "\n - %s" x))
                    (sort (cl-copy-list features) 'string<)))





(load
 (file-name-concat (file-name-directory load-file-name) "install-dependencies.el"))



(require 'org)
(require 'org-id)
(require 'org-element)
(require 'org-roam)
(require 'htmlize)

(add-hook 'org-export-before-processing-functions
          (lambda (backend)
            (when-let ((note (org-roam-node-at-point))
                       (backlinks (org-roam-backlinks-get note :unique t)))
              (save-excursion
                (goto-char (point-max))
                (insert "\n* Backlinks\n\n")
                (dolist (backlink backlinks)
                  (let* ((source-node (org-roam-backlink-source-node backlink))
                         (point (org-roam-backlink-point backlink)))
                    (insert
                     (format "- [[id:%s][%s]] (%s)\n"
                             (org-roam-node-id source-node)
                             (org-roam-node-title source-node)
                             (file-relative-name (org-roam-node-file source-node))))))))
            (message "%s" (buffer-substring-no-properties (point-min) (point-max)))))

(setf org-roam-directory "."
      org-roam-db-location "./org-roam.publish.db")

(org-roam-db-sync)

;; Trying to speed things up
(defun org-publish-ignore-mode-hooks (orig-func &rest args)
  (let ((lexical-binding nil))
    (cl-letf (((symbol-function #'run-mode-hooks) #'ignore))
      (apply orig-func args))))
(advice-add 'org-publish :around #'org-publish-ignore-mode-hooks)


;; See (describe-variable 'org-publish-project-alist)
;; See https://orgmode.org/manual/Publishing-options.html for more options
;; The force and default-directory are for interactive sessions.
(let* ((forcep t)
       ;; (org-id-link-to-org-use-id t)
       (default-directory
        (expand-file-name
         (concat
          (file-name-directory (or load-file-name buffer-file-name))
          "..")))
       (root ".")
       (project-alist
        `("public-notes"
          :base-directory ,root
          :publishing-function org-html-publish-to-html
          :publishing-directory "./public"

          :author "Francis St-Amour"
          :creator "Francis St-Amour"
          :with-author nil
          :with-date nil
          :html-validation-link nil

          :auto-sitemap t
          ;; :makeindex t
          )))
  (org-id-update-id-locations (directory-files root t "\\.org$"))
  (org-publish project-alist forcep))
