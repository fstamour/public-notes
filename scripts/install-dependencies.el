(message "Installing packages (from ELPA)")

(progn
  (package-initialize)
  (add-to-list 'package-archives
               '("melpa" . "http://melpa.org/packages/") t
               ;;   '("melpa-stable" . "http://stable.melpa.org/packages/") t
               )
  (setf package-selected-packages
        '(htmlize
          org-roam))
  (package-install-selected-packages t))

(require 'org-roam)
