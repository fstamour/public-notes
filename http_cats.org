:PROPERTIES:
:ID:       686c613c-eec8-48c6-acb0-4420d891c551
:END:
#+title: http cats

https://http.cat/

#+begin_example
Usage:

https://http.cat/[status_code]

Note: If you need an extension at the end of the URL just add .jpg.
#+end_example
